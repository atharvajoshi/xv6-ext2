/*
Very Useful Link :
    science.smith.edu/~nhowe/262/oldlabs/ext2.html
*/
#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <ext2fs/ext2_fs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define EXT2_S_IFREG 0x8000
#define EXT2_S_IFDIR 16877

#define OFFSET_BB 1024 // offset for bootblock

int BLOCK_SIZE, GROUP_SIZE, INODE_SIZE, INODES_PER_GROUP, S_LOG_BLOCK_SIZE;

#define BLOCK_OFFSET(block) (block * BLOCK_SIZE)
#define GROUP_OFFSET(group) (group * GROUP_SIZE)

#define ONE_BLOCK_REFERS (BLOCK_SIZE / sizeof(unsigned int))
#define FIRST_INDIRECT (1 + ONE_BLOCK_REFERS) //(1 + (BLOCK_SIZE / sizeof(unsigned int)))
#define SECOND_INDIRECT (FIRST_INDIRECT + ONE_BLOCK_REFERS * ONE_BLOCK_REFERS)//(FIRST_INDIRECT + (BLOCK_SIZE / sizeof(unsigned int)) * (BLOCK_SIZE / sizeof(unsigned int))) //(1 + (BLOCK_SIZE / sizeof(unsigned int)) + (BLOCK_SIZE / sizeof(unsigned int)) * (BLOCK_SIZE/sizeof(unsigned int)))
#define THIRD_INDIRECT (SECOND_INDIRECT + ONE_BLOCK_REFERS * ONE_BLOCK_REFERS * ONE_BLOCK_REFERS)
#define CMPR 0
#define PRNT 1

void read_inode(int fd, int inode_number, struct ext2_inode *inode){
    struct ext2_group_desc group;
    int group_id, inode_off;

    group_id = (inode_number - 1) / INODES_PER_GROUP;
    inode_off = (inode_number - 1) % INODES_PER_GROUP;
    // coming to the group from SEEK_SET
    // lseek64(fd, GROUP_OFFSET(group_id), SEEK_SET);
    // skipping first block from SEEK_CUR (skipping super_block if present)
    lseek64(fd, BLOCK_OFFSET(1), SEEK_SET);
    // coming to the current group descriptor
    lseek64(fd, group_id * sizeof(struct ext2_group_desc), SEEK_CUR);
    // reading current group descriptor
    read(fd, &group, sizeof(struct ext2_group_desc));

    lseek64(fd, BLOCK_OFFSET(group.bg_inode_table) + inode_off * INODE_SIZE, SEEK_SET);
    read(fd, inode, INODE_SIZE);
}

void print_dirent(struct ext2_dir_entry_2 dirent){
    printf("Inode : %d\tName : %.*s\n", dirent.inode, dirent.name_len, dirent.name);
}

int min(int a, int b){
    return a < b ? a : b;
}

int read_blocks(int fd, struct ext2_inode *inode){
    int found = 0, used_blocks = 0, block = 0, reclen = 0, off = 0, inner_b, v_inner_b, u_inner_b, count = 0;
    char buffer[BLOCK_SIZE];
    unsigned int block_num, block_i_num, block_v_i_num, size = 0;
    used_blocks = (inode->i_blocks) / (2 << S_LOG_BLOCK_SIZE);
    struct ext2_dir_entry_2 dirent;
    // printf("STUCK -1-1\n");
    size = inode->i_size;
    found = 0;
    for(block = 0; (!found) && (block < used_blocks);){
        // printf("STUCK 00\n");
        if(block < EXT2_NDIR_BLOCKS){
            lseek64(fd, BLOCK_OFFSET(inode->i_block[block]), SEEK_SET);
            if(S_ISDIR(inode->i_mode)){
                reclen = 0;
                off = 0;
                while(reclen < BLOCK_SIZE){
                    // printf("STUCK 11\n");
                    lseek64(fd, off, SEEK_CUR);
                    read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                    print_dirent(dirent);
                    off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                    reclen += dirent.rec_len;
                }
            }
            else{
                count = read(fd, &buffer, min(size, BLOCK_SIZE));
                write(STDOUT_FILENO, &buffer, count);
                size = size - count;
                // printf("%s", buffer);
            }
            block ++;
        }
        else if(block < FIRST_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);

                read(fd, &block_num, sizeof(block_num));
                lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                if(S_ISDIR(inode->i_mode)){
                    reclen = 0;
                    off = 0;
                    while(reclen < BLOCK_SIZE){
                        // printf("STUCK 11\n");
                        lseek64(fd, off, SEEK_CUR);
                        read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                        print_dirent(dirent);
                        off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                        reclen += dirent.rec_len;
                    }
                }
                else{
                    count = read(fd, &buffer, min(size, BLOCK_SIZE));
                    write(STDOUT_FILENO, &buffer, count);
                    size = size - count;
                }
                block ++;
            }
        }
        else if(block < SECOND_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS+1]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);
                read(fd, &block_num, sizeof(block_num));

                for(v_inner_b = 0; (v_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); v_inner_b ++){
                    lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                    lseek64(fd, v_inner_b * sizeof(unsigned int), SEEK_CUR);

                    read(fd, &block_i_num, sizeof(block_i_num));
                    lseek64(fd, BLOCK_OFFSET(block_i_num), SEEK_SET);
                    if(S_ISDIR(inode->i_mode)){
                        reclen = 0;
                        off = 0;
                        while(reclen < BLOCK_SIZE){
                            // printf("STUCK 11\n");
                            lseek64(fd, off, SEEK_CUR);
                            read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                            print_dirent(dirent);
                            off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                            reclen += dirent.rec_len;
                        }
                    }
                    else{
                        count = read(fd, &buffer, min(size, BLOCK_SIZE));
                        write(STDOUT_FILENO, &buffer, count);
                        size = size - count;
                    }
                    block ++;
                }
            }
        }
        else if(block < THIRD_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS+2]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);
                read(fd, &block_num, sizeof(block_num));
                for(v_inner_b = 0; (v_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); v_inner_b ++){
                    lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                    lseek64(fd, v_inner_b * sizeof(unsigned int), SEEK_CUR);
                    read(fd, &block_i_num, sizeof(block_i_num));

                    for(u_inner_b = 0; (u_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); u_inner_b ++){
                        lseek64(fd, BLOCK_OFFSET(block_i_num), SEEK_SET);
                        lseek64(fd, u_inner_b * sizeof(unsigned int), SEEK_CUR);

                        read(fd, &block_v_i_num, sizeof(block_v_i_num));
                        lseek64(fd, BLOCK_OFFSET(block_v_i_num), SEEK_SET);
                        if(S_ISDIR(inode->i_mode)){
                            reclen = 0;
                            off = 0;
                            while(reclen < BLOCK_SIZE){
                                // printf("STUCK 11\n");
                                lseek64(fd, off, SEEK_CUR);
                                read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                                print_dirent(dirent);
                                off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                                reclen += dirent.rec_len;
                            }
                        }
                        else{
                            count = read(fd, &buffer, min(size, BLOCK_SIZE));
                            write(STDOUT_FILENO, &buffer, count);
                            size = size - count;
                        }
                        block ++;
                    }
                }
            }
        }
            
    }
    return -1;
}

int compare_blocks(int fd, struct ext2_inode *inode, char *file){
    int found = 0, used_blocks = 0, block = 0, reclen = 0, off = 0, inner_b, v_inner_b, u_inner_b;
    unsigned int block_num, block_i_num, block_v_i_num;
    used_blocks = (inode->i_blocks) / (2 << S_LOG_BLOCK_SIZE);
    struct ext2_dir_entry_2 dirent;
    // printf("STUCK -1-1\n");
    found = 0;
    for(block = 0; (!found) && (block < used_blocks);){
        // printf("STUCK 00\n");
        if(block < EXT2_NDIR_BLOCKS){
            lseek64(fd, BLOCK_OFFSET(inode->i_block[block]), SEEK_SET);
            reclen = 0;
            off = 0;
            while(reclen < BLOCK_SIZE){
                // printf("STUCK 11\n");
                lseek64(fd, off, SEEK_CUR);
                read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                // printf("Block 1 : The Name = %.*s\n", dirent.name_len, dirent.name);
                if(strncmp(file, dirent.name, dirent.name_len) == 0){
                    if(dirent.name_len == strlen(file)){
                        found = 1;
                        return dirent.inode;
                    }
                }
                // printf("Name = %.*s\n", dirent.name_len, dirent.name);
                off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                reclen += dirent.rec_len;
            }
            block ++;
        }
        else if(block < FIRST_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);

                read(fd, &block_num, sizeof(block_num));
                lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                reclen = 0;
                off = 0;
                while(reclen < BLOCK_SIZE){
                    // printf("STUCK 11\n");
                    lseek64(fd, off, SEEK_CUR);
                    read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                    // printf("Block 2 : The Name = %.*s\n", dirent.name_len, dirent.name);
                    if(strncmp(file, dirent.name, dirent.name_len) == 0){
                        if(dirent.name_len == strlen(file)){
                            found = 1;
                            return dirent.inode;
                        }
                    }
                    // printf("Name = %.*s\n", dirent.name_len, dirent.name);
                    off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                    reclen += dirent.rec_len;
                }
                block ++;
            }
        }
        else if(block < SECOND_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS+1]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);
                read(fd, &block_num, sizeof(block_num));

                for(v_inner_b = 0; (v_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); v_inner_b ++){
                    lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                    lseek64(fd, v_inner_b * sizeof(unsigned int), SEEK_CUR);

                    read(fd, &block_i_num, sizeof(block_i_num));
                    lseek64(fd, BLOCK_OFFSET(block_i_num), SEEK_SET);
                    reclen = 0;
                    off = 0;
                    while(reclen < BLOCK_SIZE){
                        // printf("STUCK 11\n");
                        lseek64(fd, off, SEEK_CUR);
                        read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                        // printf("Block 3 : The Name = %.*s\n", dirent.name_len, dirent.name);
                        if(strncmp(file, dirent.name, dirent.name_len) == 0){
                            if(dirent.name_len == strlen(file)){
                                found = 1;
                                return dirent.inode;
                            }
                        }
                        // printf("Name = %.*s\n", dirent.name_len, dirent.name);
                        off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                        reclen += dirent.rec_len;
                    }
                    block ++;
                }
            }
        }
        else if(block < THIRD_INDIRECT){
            for(inner_b = 0; (inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); inner_b ++){
                lseek64(fd, BLOCK_OFFSET(inode->i_block[EXT2_NDIR_BLOCKS+2]), SEEK_SET);
                lseek64(fd, inner_b * sizeof(unsigned int), SEEK_CUR);
                read(fd, &block_num, sizeof(block_num));
                for(v_inner_b = 0; (v_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); v_inner_b ++){
                    lseek64(fd, BLOCK_OFFSET(block_num), SEEK_SET);
                    lseek64(fd, v_inner_b * sizeof(unsigned int), SEEK_CUR);
                    read(fd, &block_i_num, sizeof(block_i_num));

                    for(u_inner_b = 0; (u_inner_b < ONE_BLOCK_REFERS)&&(block < used_blocks); u_inner_b ++){
                        lseek64(fd, BLOCK_OFFSET(block_i_num), SEEK_SET);
                        lseek64(fd, u_inner_b * sizeof(unsigned int), SEEK_CUR);

                        read(fd, &block_v_i_num, sizeof(block_v_i_num));
                        lseek64(fd, BLOCK_OFFSET(block_v_i_num), SEEK_SET);
                        reclen = 0;
                        off = 0;
                        while(reclen < BLOCK_SIZE){
                            // printf("STUCK 11\n");
                            lseek64(fd, off, SEEK_CUR);
                            read(fd, &dirent, sizeof(struct ext2_dir_entry_2));
                            // printf("Block 4 : The Name = %.*s\n", dirent.name_len, dirent.name);
                            if(strncmp(file, dirent.name, dirent.name_len) == 0){
                                if(dirent.name_len == strlen(file)){
                                    found = 1;
                                    return dirent.inode;
                                }
                            }
                            // printf("Name = %.*s\n", dirent.name_len, dirent.name);
                            off = dirent.rec_len - sizeof(struct ext2_dir_entry_2);
                            reclen += dirent.rec_len;
                        }
                        block ++;
                    }
                }
            }
        }
            
    }
    return -1;
}

int get_inode(int fd, int inode_number, char *path){
    // printf("[RUNNING]\n");
    char *name;
    int i = 0, start = 0, length = strlen(path);
    struct ext2_inode inode;

    name = (char *)malloc((length + 1) * sizeof(char));
    name[length] = '\0';

    while(i < length){
        start = i;
        while(i < length && path[i] != '/')
            i ++;
        i ++;
        strncpy(name, &path[start], i-start-1);
        name[i-start-1] = '\0';
        // printf("Searching for : %s\n", name);
        read_inode(fd, inode_number, &inode);
        inode_number = compare_blocks(fd, &inode, name);
        if(inode_number < 0)
            return -1;
    }

    return inode_number;
    
    // printf("STUCK -1-1\n");
    // read_inode(fd, inode_number, &inode);
    // inode_number = compare_blocks(fd, &inode, name);
    // free(name);
    // printf("Search Results : Inode = %d\n", inode_number);
    // // Assignment/ext2.c
    // if(inode_number != -1){
    //     return get_inode(fd, inode_number, path + curr_len + 1, length - (curr_len + 1));
    // }
    // printf("[STOPPED]\n");
    return 0;
}

void print_mode(int n){
    printf("0");
    printf("%d", n / 64);
    n = n % 64;
    printf("%d", n / 8);
    n = n % 8;
    printf("%d", n);
}

void print_inode(struct ext2_inode inode, int inode_number){
	int i;
    printf("Inode: %d\t", inode_number);
    if(S_ISDIR(inode.i_mode))
        printf("Type: directory\t");
    else
        printf("Type: regular\t");
    printf("Mode: ");print_mode(inode.i_mode & ACCESSPERMS);
    printf("\tFlags: %d\nLinks: %d\tBlockcount: %d\tSize: %d\n", inode.i_flags, inode.i_links_count, inode.i_blocks, inode.i_size);
    time_t t = inode.i_atime;
    struct tm *local_time;
    local_time = localtime(&t);
    printf("atime : %s", asctime(local_time));

    t = inode.i_ctime;
    local_time = localtime(&t);
    printf("ctime : %s", asctime(local_time));

    t = inode.i_mtime;
    local_time = localtime(&t);
    printf("mtime : %s", asctime(local_time));

    printf("BLOCKS\n");
    int used_blocks = (inode.i_blocks) / (2 << S_LOG_BLOCK_SIZE);
    // if(blocks < EXT2_NDIR_BLOCKS)
    for(i = 0; (i < used_blocks) && (i < EXT2_NDIR_BLOCKS); i ++)
        printf("(%d):%d\n", i, inode.i_block[i]);
    if(used_blocks >= EXT2_NDIR_BLOCKS)
        printf("(12):%d\n", inode.i_block[12]);
    if(used_blocks >= FIRST_INDIRECT)
        printf("(13):%d\n", inode.i_block[13]);
    if(used_blocks >= SECOND_INDIRECT)
        printf("(14):%d\n", inode.i_block[14]);
    printf("TOTAL: %d\n", used_blocks);
}

int
main(int argc, char *argv[]){
    if(argc < 4){
        printf("Less Arguments\n");
        exit(1);
    }
    struct ext2_super_block sb;
    struct ext2_inode inode;
    int len = strlen(argv[2]);
    char *path = (char *)malloc((len + 1) * sizeof(char));
    strcpy(path, argv[2]);
    path[len] = '\0';

    int fd, inode_number;//, block_size, s_log_block_size;
    // printf("size of group descriptor = %ld\n", sizeof(struct ext2_group_desc));
    fd = open(argv[1], O_RDONLY);
    if(fd == -1){
        printf("File not opened\n");
        perror("Error");
        exit(1);
    }
        
    lseek(fd, OFFSET_BB, SEEK_SET);
    read(fd, &sb, sizeof(sb));
    
    /* GLOBAL DATA */
    BLOCK_SIZE = 1024 << sb.s_log_block_size;
    GROUP_SIZE = sb.s_blocks_per_group * BLOCK_SIZE;
    S_LOG_BLOCK_SIZE = sb.s_log_block_size;
    INODE_SIZE = sb.s_inode_size;
    INODES_PER_GROUP = sb.s_inodes_per_group;
    /* END OF GLOBAL DATA */
    
    // inode_number = get_inode(fd, 2, &path[1]);
    /* get inode */
    char *name;
    int i = 0, start = 0, length = strlen(path);
    // struct ext2_inode inode;

    name = (char *)malloc((length + 1) * sizeof(char));
    name[length] = '\0';
    inode_number = 2; // root directory
    path = &path[1];
    length = strlen(path);
    while(i < length){
        start = i;
        while(i < length && path[i] != '/')
            i ++;
        i ++;
        strncpy(name, &path[start], i-start-1);
        name[i-start-1] = '\0';
        // printf("Searching for : %s\n", name);
        read_inode(fd, inode_number, &inode);
        inode_number = compare_blocks(fd, &inode, name);
        if(inode_number < 0){
            printf("No such file or directory\n");
            exit(1);
        }
    }
    if(inode_number < 0){
        printf("Something Went Wrong!\n");
        exit(1);
    }

    read_inode(fd, inode_number, &inode);
    
    if(strcmp(argv[3], "data") == 0){
        // printf("data wanted\n");
        read_blocks(fd, &inode);
    }
    else if(strcmp(argv[3], "inode") == 0){
        print_inode(inode, inode_number);
        
    }
    return 0;
}







